package rpc_test

import (
	"context"
	"fmt"
	"os"
	"testing"

	"gitee.com/zuozhengjun/keyauth/apps/policy"
	"gitee.com/zuozhengjun/keyauth/apps/token"
	"gitee.com/zuozhengjun/keyauth/client/rpc"
	mcenter "github.com/infraboard/mcenter/client/rpc"
	"github.com/stretchr/testify/assert"
)

func TestBookQuery(t *testing.T) {
	should := assert.New(t)

	conf := mcenter.NewDefaultConfig()
	conf.Address = os.Getenv("MCENTER_GRPC_ADDRESS")
	conf.ClientID = os.Getenv("MCENTER_CMDB_CLINET_ID")
	conf.ClientSecret = os.Getenv("MCENTER_CMDB_CLIENT_SECRET")

	c, err := rpc.NewClient(conf)
	// c.Endpoint().RegistryEndpoint()
	// 权限校验
	// c.Policy().ValidatePermission()
	if should.NoError(err) {
		resp, err := c.Token().ValidateToken(
			context.Background(),
			token.NewValidateTokenRequest("yLYHTZTszaBF23oLoiRtVTHo"),
		)
		should.NoError(err)
		fmt.Println(resp)
	}

}

// 测试健全
func TestValidatePermission(t *testing.T) {
	should := assert.New(t)

	conf := mcenter.NewDefaultConfig()
	conf.Address = os.Getenv("MCENTER_GRPC_ADDRESS")
	conf.ClientID = os.Getenv("MCENTER_CMDB_CLINET_ID")
	conf.ClientSecret = os.Getenv("MCENTER_CMDB_CLIENT_SECRET")

	keyauthClient, err := rpc.NewClient(conf)
	if should.NoError(err) {
		req := policy.NewValidatePermissionRequest()
		req.Username = "admin"
		req.Service = "cmdb"
		req.Resource = "secret"
		req.Action = "create"

		p, err := keyauthClient.Policy().ValidatePermission(context.TODO(), req)
		if err !=nil {
			t.Fatal(err)
		}
		t.Log(p)
	}

}

func init() {
	// 提前加载好 mcenter客户端, resolver需要使用
	err := mcenter.LoadClientFromEnv()
	if err != nil {
		panic(err)
	}
}
