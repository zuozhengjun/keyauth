package auth

import (
	"gitee.com/zuozhengjun/keyauth/apps/policy"
	"gitee.com/zuozhengjun/keyauth/apps/token"
	"gitee.com/zuozhengjun/keyauth/common/utils"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/exception"
	"github.com/infraboard/mcube/http/response"
)

func (a *KeyauthAuther) RestfulAuthHandlerFunc(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	// 获取matadata 信息
	a.meta = req.SelectedRoute().Metadata()
	a.log.Debug(a.meta)

	// 判断是否开启认证
	isAuthEnable := a.getAuth()
	if isAuthEnable {

		// 1. 从请求头获取token
		accessToken := utils.GetToken(req.Request)
		// 2. 到用户中心验证
		tk, err := a.auth.ValidateToken(req.Request.Context(), token.NewValidateTokenRequest(accessToken))
		if err != nil {
			response.Failed(resp.ResponseWriter, err)
			return
		}
		// 3. 设置上下文
		req.SetAttribute("token", tk)
	}
	isPermissionEnable := a.getPermission()

	if isAuthEnable && isPermissionEnable {
		tk := req.Attribute("token").(*token.Token)
		permReq := policy.NewValidatePermissionRequest()
		permReq.Service = a.serviceName
		permReq.Username = tk.Data.UserName

		if a.meta != nil {
			if v, ok := a.meta["resource"]; ok {
				permReq.Resource = v.(string)
			}
			if v, ok := a.meta["action"]; ok {
				permReq.Action = v.(string)
			}
		}
		_, err := a.perm.ValidatePermission(req.Request.Context(), permReq)
		if err != nil {
			response.Failed(resp.ResponseWriter, exception.NewPermissionDeny(err.Error()))
			return
		}

	}

	// chain 将请求往下传下去
	chain.ProcessFilter(req, resp)

}
