package auth

import (
	"gitee.com/zuozhengjun/keyauth/apps/policy"
	"gitee.com/zuozhengjun/keyauth/apps/token"
	"gitee.com/zuozhengjun/keyauth/client/rpc"
	"github.com/infraboard/mcube/http/label"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

func NewKeyauthAuther(client *rpc.ClientSet, serviceName string) *KeyauthAuther {
	return &KeyauthAuther{
		auth:        client.Token(),
		perm:        client.Policy(),
		log:         zap.L().Named("http.auther"),
		serviceName: serviceName,
		meta:        make(map[string]interface{}),
	}
}

// 有Keyauth提供的 HTTP认证中间件
type KeyauthAuther struct {
	log         logger.Logger
	auth        token.ServiceClient
	perm        policy.RPCClient
	serviceName string
	meta        map[string]interface{}
}

func (a *KeyauthAuther) getAuth() (isAuthEnable bool) {
	if authV, ok := a.meta[label.Auth]; ok {
		switch v := authV.(type) {
		case bool:
			isAuthEnable = v
		case string:
			isAuthEnable = v == "true"
		}
	}
	return isAuthEnable
}

func (a *KeyauthAuther) getPermission() (isPermissionEnable bool) {
	if authV, ok := a.meta[label.Permission]; ok {
		switch v := authV.(type) {
		case bool:
			isPermissionEnable = v
		case string:
			isPermissionEnable = v == "true"
		}
	}
	return isPermissionEnable
}
