package utils

import (
	"math/rand"
	"net/http"
	"strings"
	"time"
)

// MakeBearer https://tools.ietf.org/html/rfc6750#section-2.1
// b64token    = 1*( ALPHA / DIGIT /"-" / "." / "_" / "~" / "+" / "/" ) *"="
func MakeBearer(lenth int) string {
	charlist := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	t := make([]string, lenth)
	rand.New(rand.NewSource(time.Now().UnixNano() + int64(lenth) + rand.Int63n(10000)))
	for i := 0; i < lenth; i++ {
		rn := rand.Intn(len(charlist))
		w := charlist[rn : rn+1]
		t = append(t, w)
	}

	token := strings.Join(t, "")
	return token
}

// 我们Token哪里获取?
// 1. URL Query String ?
// 2. Custom Header ?
// 3. Authorization Header
func GetToken(r *http.Request) (accessToken string) {

	auth := r.Header.Get("Authorization")
	al := strings.Split(auth, " ")
	if len(al) > 1 {
		accessToken = al[1]
	} else {
		// 兼容 Authorization <token>
		accessToken = auth
	}
	return
}
