package all

import (
	// 注册所有HTTP服务模块, 暴露给框架HTTP服务器加载
	_ "gitee.com/zuozhengjun/keyauth/apps/policy/api"
	_ "gitee.com/zuozhengjun/keyauth/apps/role/api"
	_ "gitee.com/zuozhengjun/keyauth/apps/token/api"
	_ "gitee.com/zuozhengjun/keyauth/apps/user/api"
)
