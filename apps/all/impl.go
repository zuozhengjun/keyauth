package all

import (
	// 注册所有GRPC服务模块, 暴露给框架GRPC服务器加载, 注意 导入有先后顺序
	_ "gitee.com/zuozhengjun/keyauth/apps/endpoint/impl"
	_ "gitee.com/zuozhengjun/keyauth/apps/policy/impl"
	_ "gitee.com/zuozhengjun/keyauth/apps/role/impl"
	_ "gitee.com/zuozhengjun/keyauth/apps/token/impl"
	_ "gitee.com/zuozhengjun/keyauth/apps/user/impl"
)
