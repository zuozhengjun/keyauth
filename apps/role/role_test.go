package role_test

import (
	"testing"

	"gitee.com/zuozhengjun/keyauth/apps/role"
)

func TestHasPermission(t *testing.T) {
	set := role.NewRoleSet()
	r := &role.Role{
		Spec: &role.CreateRoleRequest{
			Permissions: []*role.Permission{
				{
					Service: "cmdb",
					Featrues: []*role.Featrue{
						{
							Resource: "secret",
							Action:   "list",
						},
						{
							Resource: "secret",
							Action:   "get",
						},
						{
							Resource: "secret",
							Action:   "create",
						},
					},
				},
			},
		},
	}
	set.AddRole(r)

	perm, role := set.HasPermission(&role.PermissionRequest{
		Service:  "cmdb",
		Resource: "secret",
		Action:   "create",
	})
	t.Log(role)
	if perm != true {
		t.Fatal("has perm error")
	}
}
