package impl

import (
	"context"

	"gitee.com/zuozhengjun/keyauth/apps/user"
	"github.com/infraboard/mcube/pb/request"

	"gitee.com/zuozhengjun/keyauth/common/utils"
)

func (s *service) CreateUser(ctx context.Context, req *user.CreateUserRequest) (*user.User, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}

	ins := user.NewUser(req)

	ins.Data.Password = utils.HashPassword(ins.Data.Password)
	if err := s.save(ctx, ins); err != nil {
		return nil, err
	}

	return ins, nil

}

func (s *service) QueryUser(ctx context.Context, req *user.QueryUserRequest) (*user.UserSet, error) {
	query := newQueryRequest(req)
	return s.query(ctx, query)
}

func (s *service) DescribeUser(ctx context.Context, req *user.DescribeUserRequest) (*user.User, error) {
	return s.get(ctx, req)
}

func (s *service) UpdateUser(ctx context.Context, req *user.UpdateUserRequest) (*user.User, error) {

	ins, err := s.DescribeUser(ctx, user.NewDescribeUserRequestById(req.Id))
	if err != nil {
		return nil, err
	}

	switch req.UpdateMode {
	case request.UpdateMode_PUT:
		ins.Update(req)
	case request.UpdateMode_PATCH:
		err := ins.Patch(req)
		if err != nil {
			return nil, err
		}
	}

	// 校验更新后数据合法性
	if err := ins.Data.Validate(); err != nil {
		return nil, err
	}

	if err := s.update(ctx, ins); err != nil {
		return nil, err
	}

	return ins, nil
}
func (s *service) DeleteUser(ctx context.Context, req *user.DeleteUserRequest) (*user.User, error) {
	ins, err := s.DescribeUser(ctx, user.NewDescribeUserRequestById(req.Id))
	if err != nil {
		return nil, err
	}

	if err := s.deleteUser(ctx, ins); err != nil {
		return nil, err
	}

	return ins, nil
}
