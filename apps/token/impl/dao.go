package impl

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitee.com/zuozhengjun/keyauth/apps/token"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (s *service) save(ctx context.Context, token *token.Token) error {

	if _, err := s.col.InsertOne(ctx, token); err != nil {
		return exception.NewInternalServerError("inserted token(%s) document error, %s",
			token.AccessToken, err)
	}
	return nil
}

func (s *service) get(ctx context.Context, accessToken string) (*token.Token, error) {
	// 从缓存获取
	resp, err := s.redis.Get(accessToken).Bytes()
	if err != nil {
		s.log.Warnf("redis get error, %s", err)
	} else {
		tk := token.NewDefaultToken()
		if err := json.Unmarshal(resp, tk); err != nil {
			s.log.Warnf("unmarshal redis data error, %s", err)
		}
		s.log.Debugf("get token from redis %s", tk)
		return tk, nil
	}

	// 直接从数据库获取
	tk, err := s.getFromDB(ctx, accessToken)
	if err != nil {
		return nil, err
	}
	s.log.Debugf("获取 db %s", tk)
	// 设置环境
	jtk, err := json.Marshal(tk)
	if err != nil {
		s.log.Warnf("marshal token error, %s", err)
	}
	err = s.redis.Set(tk.AccessToken, string(jtk), 6*time.Minute).Err()
	if err != nil {
		s.log.Debugf("set token to redis err %s", err)
		return nil, err
	}

	return tk, nil

}

// GET, Describe
// filter 过滤器(Collection),类似于MYSQL Where条件
// 调用Decode方法来进行 反序列化  bytes ---> Object (通过BSON Tag)
func (s *service) getFromDB(ctx context.Context, accessToken string) (*token.Token, error) {
	filter := bson.M{"_id": accessToken}

	ins := token.NewDefaultToken()
	if err := s.col.FindOne(ctx, filter).Decode(ins); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, exception.NewNotFound("access token %s not found", accessToken)
		}

		return nil, exception.NewInternalServerError("find access token %s error, %s", accessToken, err)
	}

	return ins, nil
}

// delete 删除
func (s *service) delete(ctx context.Context, ins *token.Token) error {
	if ins == nil || ins.AccessToken == "" {
		return fmt.Errorf("access token is nil")
	}
	result, err := s.col.DeleteOne(ctx, bson.M{"_id": ins.AccessToken})
	if err != nil {
		return exception.NewInternalServerError("delete access token(%s) error, %s", ins.AccessToken, err)
	}

	if result.DeletedCount == 0 {
		return exception.NewNotFound("access token %s not found", ins.AccessToken)
	}

	return nil
}

// UpdateByID, 通过主键来更新对象
func (s *service) updateByID(ctx context.Context, ins *token.Token) error {
	// SQL update obj(SET f=v,f=v) where id=?
	// s.col.UpdateOne(ctx, filter(), ins)
	if _, err := s.col.UpdateByID(ctx, ins.AccessToken, ins); err != nil {
		return exception.NewInternalServerError("update token(%s) document error, %s",
			ins.AccessToken, err)
	}

	return nil
}
