package impl

import (
	"context"
	"fmt"
	"time"

	"gitee.com/zuozhengjun/keyauth/apps/token"
	"gitee.com/zuozhengjun/keyauth/apps/user"
	"gitee.com/zuozhengjun/keyauth/common/utils"
	"github.com/infraboard/mcube/exception"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	AUTH_ERROR           = "user or password not correct"
	DefaultTokenDuration = 10 * time.Minute
)

func (s *service) IssueToken(ctx context.Context, req *token.IssueTokenRequest) (*token.Token, error) {
	// 验证密码 颁发Token
	if err := req.Validate(); err != nil {
		return nil, exception.NewBadRequest("validate issue token error, %s", err)
	}
	switch req.GranteType {
	case token.GranteType_PASSWORD:
		// 1. 获取用户对象(User Object)
		descReq := user.NewDescribeUserRequestByName(req.UserDomain, req.UserName)
		u, err := s.user.DescribeUser(ctx, descReq)
		s.log.Debugf("describe user %s", u)
		if err != nil {
			s.log.Debug("describe user error, %s", err)
			if exception.IsNotFoundError(err) {
				// 屏蔽返回错误
				return nil, exception.NewUnauthorized(AUTH_ERROR)
			}
			return nil, err
		}
		if ok := u.CheckPassword(req.Password); !ok {
			return nil, exception.NewUnauthorized(AUTH_ERROR)
		}

		// 3. 颁发一个Token, 颁发<JWT> xxx  Sign(url+ body) Sing-->Heander -->  Hash
		// 4. rfc: Bearer  字符串:   Header: Authorization  Header Value: bearer <access_token>
		tk := token.NewToken(req, DefaultTokenDuration)

		tk.Data.Password = ""
		s.log.Debugf("issue token %s", tk)

		// 6. 入库持久化
		if err := s.save(ctx, tk); err != nil {
			return nil, err
		}
		return tk, nil

	default:
		return nil, fmt.Errorf("grant type %s not implemented", req.GranteType)
	}

}

// RevolkToken 撤销Token(Logout)
func (s *service) RevolkToken(ctx context.Context, req *token.RevolkTokenRequest) (*token.Token, error) {
	// 1. 获取 access_token
	tk, err := s.get(ctx, req.AccessToken)
	if err != nil {
		return nil, err
	}
	// 2. 检查RefreshToken是否匹配

	if tk.RefreshToken != req.RefreshToken {
		return nil, exception.NewBadRequest("refresh token not conrrect")
	}

	// 3. 删除 access_token
	if err := s.delete(ctx, tk); err != nil {
		return nil, err
	}

	return tk, nil
}

// 校验Token的接口(内部服务使用)
func (s *service) ValidateToken(ctx context.Context, req *token.ValidateTokenRequest) (*token.Token, error) {
	// 1. 获取 access_token
	tk, err := s.get(ctx, req.AccessToken)

	if err != nil {
		return nil, err
	}

	// 2. 校验 access_token 合法性
	if err := tk.Validate(); err != nil {
		// 2.1 如果Aceess Token过期
		if utils.IsAccessTokenExpiredError(err) {
			if tk.IsRefreshTokenExpired() {
				return nil, exception.NewRefreshTokenExpired("refresh token expired")
			}
			// 2.2 如果Refresh没过期, 可以续过期时间
			tk.Extend(DefaultTokenDuration)
			// 往数据库更新过期时间
			if err = s.updateByID(ctx, tk); err != nil {
				return nil, err
			}
		}
		return nil, err
	}

	// 3. 返回
	return tk, nil
}
func (s *service) QueryToken(ctx context.Context, req *token.QueryTokenRequest) (*token.TokenSet, error) {
	return nil, status.Errorf(codes.Unimplemented, "method QueryToken not implemented")
}
