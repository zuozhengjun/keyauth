package token

import (
	"fmt"
	"time"

	"gitee.com/zuozhengjun/keyauth/apps/user"
	"gitee.com/zuozhengjun/keyauth/common/utils"
	"github.com/infraboard/mcube/exception"
)

const (
	AppName = "token"
)

func (req *IssueTokenRequest) Validate() error {
	switch req.GranteType {
	case GranteType_PASSWORD:
		if req.UserName == "" || req.Password == "" {
			return fmt.Errorf("password grant required user_name and password")
		}
	}

	return nil
}

func NewToken(req *IssueTokenRequest, expiredDuration time.Duration) *Token {
	now := time.Now()
	expired := now.Add(expiredDuration)
	refresh := now.Add(expiredDuration * 5)

	return &Token{
		// 唯一ID
		AccessToken: utils.MakeBearer(24),
		// 颁发时间
		IssueAt: now.UnixMilli(),
		// 颁发请求
		Data: req,
		// Access Token过期时间(绝对时间), 10分钟,  now() + 10分钟
		AccessTokenExpiredAt: expired.UnixMilli(),
		// token过期了, 允许刷新
		RefreshToken: utils.MakeBearer(32),
		// Access Token过期时间(绝对时间), 10分钟,  now() + 10分钟
		RefreshTokenExpiredAt: refresh.UnixMilli(),
	}
}

func NewIssueTokenRequest() *IssueTokenRequest {
	return &IssueTokenRequest{
		UserDomain: user.DefaultDomain,
	}
}

func NewDefaultToken() *Token {
	return &Token{
		Data: &IssueTokenRequest{},
		Meta: map[string]string{},
	}
}

func (t *Token) Validate() error {
	// 判断Token过期没有
	// 是一个时间戳,
	//  now   expire
	if time.Now().UnixMilli() > t.AccessTokenExpiredAt {
		return exception.NewAccessTokenExpired("access token expired")
	}

	return nil
}

func (t *Token) IsRefreshTokenExpired() bool {
	// 判断refresh Token过期没有
	// 是一个时间戳,
	//  now   expire
	if time.Now().UnixMilli() > t.RefreshTokenExpiredAt {
		return true
	}

	return false
}

// 续约Token, 延长一个生命周期
func (t *Token) Extend(expiredDuration time.Duration) {
	now := time.Now()
	// Token 10
	expired := now.Add(expiredDuration)
	refresh := now.Add(expiredDuration * 5)

	t.AccessTokenExpiredAt = expired.UnixMilli()
	t.RefreshTokenExpiredAt = refresh.UnixMilli()
}

func NewRevolkTokenRequest() *RevolkTokenRequest {
	return &RevolkTokenRequest{}
}

func NewValidateTokenRequest(at string) *ValidateTokenRequest {
	return &ValidateTokenRequest{
		AccessToken: at,
	}
}
